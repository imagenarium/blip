<@requirement.NODE ref='blip' primary='blip-${namespace}' single='false' />

<@requirement.PARAM name='API_PORT' required='false' type='port' scope='global' />
<@requirement.PARAM name='MODEL_SIZE' type='select' values='base,large' value='base' scope='global' />

<@img.TASK 'blip-${namespace}' 'imagenarium/blip:1.0-cpu'>
  <@img.NODE_REF 'blip' />
  <@img.VOLUME '/root/.cache' />
  <@img.PORT PARAMS.API_PORT '80' />
  <@img.ENV 'MODEL_SIZE' PARAMS.MODEL_SIZE />
  <@img.CHECK_PORT '80' />
</@img.TASK>
