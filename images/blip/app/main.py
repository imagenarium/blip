from transformers import BlipProcessor, BlipForConditionalGeneration
from PIL import Image
from fastapi import FastAPI, UploadFile
import os
import io
import uvicorn


model_size = os.environ['MODEL_SIZE']
app = FastAPI()


@app.on_event("startup")
def startup_event():
    app.processor = BlipProcessor.from_pretrained("Salesforce/blip-image-captioning-" + model_size)
    app.model = BlipForConditionalGeneration.from_pretrained("Salesforce/blip-image-captioning-" + model_size)


@app.post("/generate", name="Сгенерировать текст по изображению", tags=["AI Vision"])
def generate(file: UploadFile, temp: float = 0.5, top_p: float = 0.5):
    image = Image.open(io.BytesIO(file.file.read())).convert('RGB')
    inputs = app.processor(image, return_tensors="pt")
    generated_ids = app.model.generate(pixel_values=inputs.pixel_values,
                                       max_new_tokens=512,
                                       repetition_penalty=1.0,
                                       do_sample=True,
                                       temperature=temp,
                                       top_k=0,
                                       top_p=top_p)
    blip_text = app.processor.batch_decode(generated_ids, skip_special_tokens=True)[0]
    return {"result": blip_text}


if __name__ == "__main__":
    config = uvicorn.Config("main:app",
                            port=80,
                            host="0.0.0.0",
                            log_level="info",
                            workers=os.cpu_count())
    server = uvicorn.Server(config)
    server.run()